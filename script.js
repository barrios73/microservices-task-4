function getUserInfo(){

    const uri = "https://jsonplaceholder.typicode.com/users"

    fetch(uri, {
      method: 'GET'
      })
      .then(response => response.json())
      .then(data => {
          displayUserInfo(data);
      })
      .catch(error => console.error('Unable to add student', error));
  }


  function displayUserInfo(data){
    const tBody = document.getElementById('userInfoTbody');
    tBody.innerHTML = '';

    data.forEach(item => {
  
        let tr = tBody.insertRow();
        
        let td1 = tr.insertCell(0);
        let textIdNode = document.createTextNode(item.id);
        td1.appendChild(textIdNode);
    
        let td2 = tr.insertCell(1);
        let textNameNode = document.createTextNode(item.name);
        td2.appendChild(textNameNode);
    
        let td3 = tr.insertCell(2);
        let textUserNameNode = document.createTextNode(item.username);
        td3.appendChild(textUserNameNode);
  
        let td4 = tr.insertCell(3);
        let textEmailNode = document.createTextNode(item.email);
        td4.appendChild(textEmailNode);
  
        let td5 = tr.insertCell(3);
        let textWebsiteNode = document.createTextNode(item.website);
        td4.appendChild(textWebsiteNode);
        
      });
    
      studentdata = data;
  }